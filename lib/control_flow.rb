# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete(str.downcase)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid_idx = str.length / 2
  if str.length.even?
    return str[mid_idx - 1] + str[mid_idx]
  else
    return str[mid_idx]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.chars { |char| count += 1 if VOWELS.include?(char)}
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = []
  num.downto(1) { |n| result << n }
  result.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""
  arr.each_with_index do |el, idx|
    if idx == arr.length - 1
      string += el
    else
      string += el + separator
    end
  end
  string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = ""
  str.each_char.with_index do |letter, idx|
    if idx.even?
      result += letter.downcase
    else
      result += letter.upcase
    end
  end
  result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split
  result = words.map { |word| word.length >= 5 ? word.reverse : word }
  result.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  1.upto(n) do |num|
    if num % 3 == 0 && num % 5 == 0
      result << "fizzbuzz"
    elsif num % 3 == 0
      result << "fizz"
    elsif num % 5 == 0
      result << "buzz"
    else
      result << num
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  (arr.length - 1).downto(0) do |index|
    result << arr[index]
  end
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  2.upto(num - 1) { |factor| return false if num % factor == 0 }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  1.upto(num) { |factor| result << factor if num % factor == 0 }
  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors = factors(num)
  factors.select { |factor| prime?(factor) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors = prime_factors(num)
  prime_factors.length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_nums = []
  even_nums = []
  arr.each do |num|
    if num.odd?
      odd_nums << num
    elsif num.even?
      even_nums << num
    end
  end
  if odd_nums.length > even_nums.length
    return even_nums[0]
  else
    return odd_nums[0]
  end
end
